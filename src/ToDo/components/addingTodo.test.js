import React from 'react';
import {
    act, render,
    screen,
    waitForElementToBeRemoved
} from "@testing-library/react";
import {Main} from "./main";
import firebase from "firebase/compat";
import userEvent from "@testing-library/user-event";

firebase.initializeApp({
    apiKey: "AIzaSyBdXi7qAucBHcqNJ8sb73ayHstruw8w94w",
    authDomain: "xaxa-todo.firebaseapp.com",
    databaseURL: "https://xaxa-todo-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "xaxa-todo",
    storageBucket: "xaxa-todo.appspot.com",
    messagingSenderId: "920237973099",
    appId: "1:920237973099:web:2facf494e0d9818e495b2e",
    measurementId: "G-G21E1J5GFF"
});

describe('Добавление дела', () => {

    it('Ввод текста в форму', async () => {
        act(() => render(<Main db={firebase.firestore()} />))

        userEvent.type(screen.getByLabelText('todo-input'), 'test')
        console.log('В поле ввода содержится текст: ', screen.getByLabelText('todo-input').value)
        expect(screen.getByLabelText('todo-input').value).toContain('test')
    });

    it('Добавление нового элемента к списку дел', async () => {
        act(() => render(<Main db={firebase.firestore()} />)) //Рендерим компонент Main и передаём ему ч/з пропсы firebase.firestore, после инициализации приложения

        userEvent.type(screen.getByLabelText('todo-input'), 'test') //Выполняем ввод текста 'test' в объект, у к/шл aria-label="todo-input" и "нажимаем' Enter
        if (screen.getByLabelText('todo-input').value === '') //Проверяем изчиел ли текст из поля ввода, т к при нажатии клавиши Enter он должен изчезать
            console.log('Текст из поля ввода изчез') //Ложим в консоль о данном деянии - пока всё ок
        const text = await screen.findByText('test') //Но когда выполняем поиск по тексту 'тест' в компоненте, ничего не находим.
        // Благодаря консоли и ошибками firebase понимаем, что данные не отправились в базу => и заметка не появилась
    });

});